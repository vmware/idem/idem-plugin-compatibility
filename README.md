# !ARCHIVED!

This project has been archived, along with all other POP and Idem-based projects.
- For more details: [Salt Project Blog - POP and Idem Projects Will Soon be Archived](https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/)

This is a test repository, used to verify idem plugin compatibility.

Currently, the repository verifies that the current plugins can simultaneously be installed.

It then runs `idem --version` to verify that plugins successfully load.
